﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using DAL.Entities;

namespace DAL.Contexts
{
    public class ItemsContext : DbContext
    {
        public DbSet<Item> Items { get; set; }

        static ItemsContext()
        {
            Database.SetInitializer <ItemsContext> (new ItemsContextInitializer());
        }

        public ItemsContext() : base("ItemConnection")
        {

        }
    }
}
