﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DAL.Repositories;

namespace CheckTheDALLayer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ItemRepository repos = new ItemRepository())
            {
                foreach (var item in repos.GetAllEntities())
                {
                    if (item != null)
                    {
                        Console.WriteLine("Item name - {0}, item type - {1}", item.ItemName, item.ItemType);
                    }
                    else
                    {
                        Console.WriteLine("The dataset wasn't received :_(");
                    }
                }
            }
        }
    }
}

