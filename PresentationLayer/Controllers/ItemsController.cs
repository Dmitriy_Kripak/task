﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DAL.Entities;
using DAL.Repositories;
using System.Web.Http.Cors;

namespace PresentationLayer.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ItemsController : ApiController
    {
        ItemRepository repos = new ItemRepository();
         
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(repos.GetAllEntities());
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var item = this.repos.GetConcrete(id);
            if (item != null)
                return Ok(item);

            return NotFound();
        }

        [HttpPost]
        public IHttpActionResult Post([FromBody]Item item)
        {
            this.repos.Create(item);
            this.repos.Save();
            return Ok(item);
        }

        [HttpPut]
        public IHttpActionResult Put(int id, [FromBody]Item item)
        {
            var itemTemp = this.repos.GetConcrete(item.Id);
            if (itemTemp != null)
            {
                itemTemp.Id = item.Id;
                itemTemp.ItemName = item.ItemName;
                itemTemp.ItemType = item.ItemType;

                this.repos.Update(itemTemp);

                return Ok(itemTemp);
            }
            return NotFound();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var itemTemp = this.repos.GetConcrete(id);

            if(itemTemp != null)
            {
                this.repos.Delete(itemTemp);
                this.repos.Save();

                return Ok(itemTemp);
            }
            return NotFound();
        }
    }
}