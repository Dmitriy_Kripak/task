﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DAL.Interfaces;
using DAL.Entities;
using DAL.Contexts;
using System.Data.Entity;

namespace DAL.Repositories
{
    public class ItemRepository : IRepository<Item>
    {
        ItemsContext context;

        public ItemRepository()
        {
            this.context = new ItemsContext();
        }

        public Item Create(Item item)
        {
            if (item != null)
            {
                this.context.Items.Add(item);
                return item;
            }
            else
                return null;
        }

        //check cascading delete related comments
        public void Delete(Item item)
        {
            Item itemTemp = this.context.Items.Find(item.Id);
            if (itemTemp != null)
            {
                this.context.Items.Remove(itemTemp);
            }
        }

        public IEnumerable<Item> GetAllEntities()
        {
            return this.context.Items.ToList();
        }

        public Item GetConcrete(int id)
        {
            return this.context.Items.Find(id);
        }

        public void Save()
        {
            this.context.SaveChanges();
        }

        //Will be called after making changes
        //to an existing entity object
        public void Update(Item item)
        {
            this.context.Entry(item).State = EntityState.Modified;
            this.Save();
        }

        public void Dispose()
        {
            this.context.Dispose();
        }
    }
}


