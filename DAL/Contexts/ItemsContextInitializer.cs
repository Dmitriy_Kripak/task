﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using DAL.Entities;

namespace DAL.Contexts
{
    class ItemsContextInitializer : DropCreateDatabaseAlways<ItemsContext>
    {
        protected override void Seed(ItemsContext context)
        {
            List<Item> itemsInit = new List<Item>();

            Item item1 = new Item() { ItemName = "MacBook", ItemType = "Laptop"};
            itemsInit.Add(item1);
            Item item2 = new Item() { ItemName = "MacBook Air 13\"", ItemType = "Laptop" };
            itemsInit.Add(item2);
            Item item3 = new Item() { ItemName = "MacBook Pro 15\"", ItemType = "Laptop" };
            itemsInit.Add(item3);
            Item item4 = new Item() { ItemName = "iPad Pro 12,9\"", ItemType = "IPad" };
            itemsInit.Add(item4);
            Item item5 = new Item() { ItemName = "iPad Pro 10,5\"", ItemType = "IPad" };
            itemsInit.Add(item5);
            Item item6 = new Item() { ItemName = "iPad", ItemType = "IPad" };
            itemsInit.Add(item6);
            Item item7 = new Item() { ItemName = "iPad mini 4", ItemType = "IPad" };
            itemsInit.Add(item7);
            Item item8 = new Item() { ItemName = "iPhone X", ItemType = "IPhone" };
            itemsInit.Add(item8);
            Item item9 = new Item() { ItemName = "iPhone 8 Plus", ItemType = "IPhone" };
            itemsInit.Add(item9);
            Item item10 = new Item() { ItemName = "iPhone 8", ItemType = "IPhone" };
            itemsInit.Add(item10);
            Item item11 = new Item() { ItemName = "iPhone 7 Plus", ItemType = "IPhone" };
            itemsInit.Add(item11);
            Item item12 = new Item() { ItemName = "iPhone 7", ItemType = "IPhone" };
            itemsInit.Add(item12);

            context.Items.AddRange(itemsInit);
            context.SaveChanges();

            base.Seed(context);
        }
    }
}
