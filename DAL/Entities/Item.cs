﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Item
    {
        [Key]
        public int Id { get; set; }

        public string ItemName { get; set; }

        public string ItemType { get; set; }
    }
}
