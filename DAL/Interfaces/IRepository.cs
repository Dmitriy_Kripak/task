﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepository<T> : IDisposable
          where T : new ()
    {
        // Getting all entities for current game
        IEnumerable<T> GetAllEntities();
        // Getting one entity by Id
        T GetConcrete(int id);
        // Adding one "Identifier" to DB
        T Create(T item);
        // Update concrete entity
        void Update(T item);
        // Delete concrete entity
        void Delete(T item);
        // Save the changes in the DB
        void Save();
    }
}
